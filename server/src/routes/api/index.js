const severity = require( "./severity" );
const errors = require( "./errors" );
const catchphrase = require( "./catchphrase" );

module.exports.register = async server => {
   await severity.register( server );
   await errors.register( server );
   await catchphrase.register( server );
};