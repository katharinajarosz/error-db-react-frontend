const utils = require( "../utils" );

const register = async ({ sql, getConnection }) => {
    // read in all the .sql files for this folder
    const sqlQueries = await utils.loadSqlQueries( "severity" );


    const getSeverity = async () => {
        // get a connection to SQL Server
        const cnx = await getConnection();

        // create a new request
        const request = await cnx.request();

        // return the executed query
        return request.query(sqlQueries.getSeverity);
    };

    const addSeverity = async () => {

    };

    const updateSeverity = async () => {

    };

    const deleteSeverity = async () => {

    };

    return {
        getSeverity
    };
};

module.exports = { register };