SELECT [Error Number (Polarion)] AS id,
    [Error-ID] AS error_id,
    [Error Text (User)] AS desc_user,
    [Solution for User] AS sol_user,
    [Error Text (Service)] AS desc_service,
    [Solution for Service] AS sol_service,
    [Severity] AS severity,
    [Abort] AS abortion,
    [Deprecated] AS deprecated
FROM [dbo].[ListOfSystemErrors_deu_complete];