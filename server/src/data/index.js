const conn = require("./connection");
const severity = require("./severity");
const errors = require("./errors");
const catchphrase = require("./catchphrase");
const sql = require("mssql");

const client = async (server, config) => {

    const getConnection = await conn(server, config);

    // this is the API the client exposes to the rest
    // of the application
    return {
        severity: await severity.register({ sql, getConnection }),
        errors: await errors.register({ sql, getConnection }),
        catchphrase: await catchphrase.register({ sql, getConnection })
    };
};

module.exports = client;