INSERT INTO [dbo].[catchphrase_translation]
           ([iso_369_3_id]
           ,[translation]
           ,[description])
     VALUES
           (@isoId
           ,@translation
           ,@description)
SELECT SCOPE_IDENTITY() AS id;