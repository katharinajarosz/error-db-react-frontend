SELECT [translation],
    [description]
FROM [AdvosBITest].[dbo].[catchphrase_translation]
WHERE [iso_369_3_id] = 'deu'
ORDER BY [translation];