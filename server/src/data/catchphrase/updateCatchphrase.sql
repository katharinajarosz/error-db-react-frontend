UPDATE [dbo].[catchphrase_translation]
SET [translation] = @translation,
    [description] = @description
WHERE [catchphrase_id] = @catchphraseId
    AND [iso_369_3_id] = @isoId;
    
SELECT [translation],
    [description]
FROM [dbo].[catchphrase_translation]
WHERE [catchphrase_id] = @catchphraseId
    AND [iso_369_3_id] = @isoId;