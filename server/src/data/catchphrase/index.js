const utils = require("../utils");


const register = async ({ sql, getConnection }) => {
    // read in all the .sql files for this folder
    const sqlQueries = await utils.loadSqlQueries("catchphrase");

    const getCatchphrase = async () => {
        // get a connection to SQL Server
        const pool = await getConnection();

        // create a new request
        const request = await pool.request();

        // return the executed query
        return request.query(sqlQueries.getCatchphrase);
    };

    const addCatchphrase = async () => {

    };

    const updateCatchphrase = async () => {

    };

    const deleteCatchphrase = async () => {

    };

    return {
        getCatchphrase
    };
};

module.exports = { register };
