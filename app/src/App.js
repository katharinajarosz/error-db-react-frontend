import './App.css';
import * as React from "react";
import { Button } from '@material-ui/core';
import { DefaultButton, Stack, Dropdown } from "office-ui-fabric-react";
import { getErrors, getCatchphrase } from "./services/Service";
import { ReactTabulator } from 'react-tabulator';

const verticalGapStackTokens = {
  childrenGap: 10,
  padding: 10,
};
const dropdownStyles = {
  dropdown: { width: 300 }
};

const filterColumns = [
  { formatter:"rownum", hozAlign:"center", width:40 },
  { field: 'id', title: 'POLARION ID', editor: 'input', headerFilter: 'input' },
  { field: 'error_id', title: 'ERROR ID', editor: 'input', headerFilter: 'input' },
  { field: 'desc_user', title: 'DESCRIPTION FOR USER', formatter: 'textarea' },
  { field: 'sol_user', title: 'SOLUTION FOR USER', formatter: 'textarea' },
  { field: 'desc_service', title: 'DESCRIPTION FOR SERVICE', formatter: 'textarea' },
  { field: 'sol_service', title: 'SOLUTION FOR SERVICE', formatter: 'textarea' },
  { field: 'severity', title: 'SEVERITY', editor: 'input', headerFilter: 'input' },
  { field: 'abortion', title: 'ABORTION', editor: 'input', headerFilter: 'input' },
  { field: 'deprecated', title: 'DEPRECATED', editor: 'input', headerFilter: 'input' }
];

export default class App extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      showTable: false,
      rows: [],
      columns: [],
      dropdownOptions: [],
      layout: ""
    };
  }

  componentDidMount() {
    this.setDropdown().then(() => { });
  }

  createErrorTable = async () => {
    getErrors()
      .then(response => {
        const errorCol = [
          { formatter:"rownum", hozAlign:"center", width:40 },
          { field: 'id', title: 'POLARION ID' },
          { field: 'error_id', title: 'ERROR ID' },
          { field: 'desc_user', title: 'DESCRIPTION FOR USER' },
          { field: 'sol_user', title: 'SOLUTION FOR USER', formatter: 'textarea' },
          { field: 'desc_service', title: 'DESCRIPTION FOR SERVICE' },
          { field: 'sol_service', title: 'SOLUTION FOR SERVICE' },
          { field: 'severity', title: 'SEVERITY' },
          { field: 'abortion', title: 'ABORTION' },
          { field: 'deprecated', title: 'DEPRECATED' }
        ];
        let errorRows = [];
        response.forEach(item => {
          errorRows.push({
            id: item.id,
            error_id: item.error_id,
            desc_user: item.desc_user,
            sol_user: item.sol_user,
            desc_service: item.desc_service,
            sol_service: item.sol_service,
            severity: item.severity,
            abortion: item.abortion,
            deprecated: item.deprecated
          });
        });
        this.setState({ columns: filterColumns });
        this.setState({ rows: errorRows });
        this.setState({ showTable: true });
      }, err => {
        console.log(err);
      })
  }

  setDropdown = async () => {
    getCatchphrase()
      .then(response => {
        let options = [];
        response.forEach(item => {
          let option = {};
          option.key = item.id;
          option.text = item.translation;
          options.push(option);
        });
        this.setState({ dropdownOptions: options });

      }, err => {
        console.log(err);
      })
  };

  render() {
    
    const options = {
      height: 800,
      layout: "fitColumns"
    };
    return (
      <div>
        <Button
          variant="contained"
          onClick={this.createErrorTable.bind(this)}>Create Error Table
          </Button>
        <Stack tokens={verticalGapStackTokens}>
          <Dropdown
            label="Catchphrase"
            multiSelect
            options={this.state.dropdownOptions}
            styles={dropdownStyles}
          />
        </Stack>
        <div>{this.state.showTable && <ReactTabulator
          columns={this.state.columns}
          data={this.state.rows}
          options={options}
        />}</div>
      </div>
    );
  }
}


